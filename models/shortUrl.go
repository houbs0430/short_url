package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

type ShortUrlModel struct {
	Id        int64  `orm:pk` // id默认是主键，而且自增
	ShortUrl  string `orm:size(500)`
	OriginUrl string `orm:size(500)`
	HashCode  string `orm:size(500)`
}

// 初始化
func init() {
	orm.RegisterDataBase("default", "mysql", "root:123456@tcp(192.168.0.114:3306)/short_url?parseTime=true&timeout=1s")

	orm.RegisterModel(new(ShortUrlModel))

	orm.RunSyncdb("default", false, true)
}

func insert() {
	newOrm := orm.NewOrm()

	urlModel := new(ShortUrlModel)

	urlModel.OriginUrl = "http://www.baidu.com"

	id, err := newOrm.Insert(&urlModel)

	if err != nil {
		beego.Info("insert into error")
	}
	logs.Info("insert into success, id:", id)
}
