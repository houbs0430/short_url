module short_url

go 1.14

require (
	github.com/astaxie/beego v1.12.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.1-0.20200324155115-ee514944af4b
	github.com/mattn/go-gtk v0.0.0-20191030024613-af2e013261f5
	github.com/mattn/go-pointer v0.0.0-20190911064623-a0a44394634f // indirect
	github.com/shiena/ansicolor v0.0.0-20151119151921-a422bbe96644 // indirect
	github.com/smartystreets/goconvey v1.6.4
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
