package main

import (
	"fmt"
	"os"
	_ "short_url/routers"

	_ "github.com/astaxie/beego"
	"github.com/mattn/go-gtk/gtk"
)

func main() {
	//beego.Run()
	// 初始化
	gtk.Init(&os.Args)

	// 创建窗口，设置属性
	win := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	win.SetTitle("tile")
	win.SetSizeRequest(400, 380)
	win.Show()

	// 启动
	gtk.Main()
	fmt.Printf("aa")
}
